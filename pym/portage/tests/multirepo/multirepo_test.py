#!/usr/bin/python

import sys, os
import os.path as osp

#sys.path.insert(0, osp.dirname(osp.dirname(osp.abspath(__file__))))
import portage
from  _emerge.actions import action_build
from _emerge.stdout_spinner import stdout_spinner
from portage.package.ebuild.config import config
from portage.util.mtimedb import MtimeDB
from _emerge.RootConfig import RootConfig
from portage.sets import load_default_config
from cStringIO import StringIO

def load_emerge_config2 (trees=None, config_root=None):
	kwargs = {}
	for k, envvar in (("config_root", "PORTAGE_CONFIGROOT"), ("target_root", "ROOT")):
		v = os.environ.get(envvar, None)
		if v and v.strip():
			kwargs[k] = v
	trees = portage.create_trees(trees=trees, config_root=config_root,  **kwargs)

	for root, root_trees in trees.items():
		settings = root_trees["vartree"].settings
		settings._init_dirs()
		setconfig = load_default_config(settings, root_trees)
		root_trees["root_config"] = RootConfig(settings, root_trees, setconfig)

	settings = trees["/"]["vartree"].settings

	for myroot in trees:
		if myroot != "/":
			settings = trees[myroot]["vartree"].settings
			break

	mtimedbfile = os.path.join(os.path.sep, settings['ROOT'], portage.CACHE_PATH, "mtimedb")
	mtimedb = portage.MtimeDB(mtimedbfile)
	portage.output._init(config_root=settings['PORTAGE_CONFIGROOT'])
	return settings, trees, mtimedb

class MultirepoTest():
	def runEmergeTest(self, function, *params):
		"""
		Run the function and return the text written in stdout
		"""
		output = sys.stdout
		output_err = sys.stderr
		sys.stdout = os.tmpfile()
		sys.stderr = os.tmpfile()
		function(*params)
		sys.stdout.flush()
		sys.stdout.seek(0)
		sys.stderr.flush()
		sys.stderr.seek(0)
		return_str = sys.stdout.read() + sys.stderr.read()
		sys.stdout.close()
		sys.stdout = output
		sys.stderr.close()
		sys.stderr = output_err
		return return_str

	def __init__(self):
		localdir = osp.dirname(osp.abspath(__file__))
		self.spinner = stdout_spinner()
		self.spinner.update = self.spinner.update_basic  #nospinner
		portage.output.havecolor = 0

		buffer = file(osp.join(localdir, 'etc', 'portage', 'repos.conf.sample')).read()
		buffer = buffer.replace("LOCATION=", "LOCATION=" + localdir + osp.sep)
		f = file (osp.join(localdir, 'etc', 'portage', 'repos.conf'), 'w')
		f.write(buffer)
		f.close()

		self.settings, self.trees, self.mtimedb = load_emerge_config2(config_root=localdir)

	def __del__(self):
		localdir = osp.dirname(osp.abspath(__file__))
		os.unlink(osp.join(localdir, 'etc', 'portage', 'repos.conf'))
	
	def run(self):
		myfiles= ["genlop"]
		runEmergeTest(action_build, "", self.settings, self.trees, self.mtimedb, self.myopts, None, myfiles, self.spinner)

