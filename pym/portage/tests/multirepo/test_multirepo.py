# test_dep_getslot.py -- Portage Unit Testing Functionality
# Copyright 2006 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
import re
from portage.tests import TestCase
from portage.dep import dep_getrepo
from multirepo_test import MultirepoTest
from  _emerge.actions import action_build

class testRepositorySelection(TestCase):
	""" A simple testcase for isvalidatom
	"""

	def testRepositorySelection(self):
		myopts ={'--pretend': True, '--verbose': True, '--color': 'n'}
		
		tests= {
			#Select repository using ::
			"test-repo-selection::gentoo": "(.|\n)*cat-test/test-repo-selection-2.0.*0 kB\n",
			"test-repo-selection::repo1": "(.|\n)*cat-test/test-repo-selection-2.0.*[1](.|\n)*[1].*ebuilds/repo1",
			"test-repo-selection::repo2": "(.|\n)*cat-test/test-repo-selection-2.0.*[1](.|\n)*[1].*ebuilds/repo2",
			"=test-repo-selection-1.0::gentoo": "(.|\n)*cat-test/test-repo-selection-1.0.*0 kB\n",
			"=test-repo-selection-1.0::repo1": "(.|\n)*cat-test/test-repo-selection-1.0.*[1](.|\n)*[1].*ebuilds/repo1",
			"=test-repo-selection-1.0::repo2": "(.|\n)*cat-test/test-repo-selection-1.0.*[1](.|\n)*[1].*ebuilds/repo2",

       			#Select repository for packages in only one repo
			"test-repo-unique": "(.|\n)*cat-test/test-repo-unique-1.0.*[1](.|\n)*[1].*ebuilds/repo1",
			"test-repo-unique-portage": "(.|\n)*cat-test/test-repo-unique-portage-1.0.*0 kB\n",
       			#Package in repository 1 and 2, but 1 must be used
			"=test-repo-almost-unique-1.0": "(.|\n)*cat-test/test-repo-almost-unique-1.0.*[1](.|\n)*[1].*ebuilds/repo1",

			#cpv just in repo2
			"=test-repo-selection-1.1": "(.|\n)*cat-test/test-repo-selection-1.1.*[1](.|\n)*[1].*ebuilds/repo2",
			#cpv just in portage
			"=test-repo-selection-1.2": "(.|\n)*cat-test/test-repo-selection-1.2.*0 kB\n",
			#cpv in repo1 and 2, but 1 must be used
			"=test-repo-selection-1.3": "(.|\n)*cat-test/test-repo-selection-1.3.*[1](.|\n)*[1].*ebuilds/repo1",
       		
	       		#package in portage and other repo
			#There is a bug related to next line
			"test-repo-selection": "(.|\n)*cat-test/test-repo-selection-2.0.*[1](.|\n)*[1].*ebuilds/repo1",

			#Package in other slot
			"test-repo-selection:2": "(.|\n)*cat-test/test-repo-selection-1.4.*[1](.|\n)*[1].*ebuilds/repo2",
			#2 cpvs in diferent repos and with the same slot. Try to select the correct one
			#There is a bug related to next line
			#"test-repo-selection:3": "(.|\n)*cat-test/test-repo-selection-1.5.*[1](.|\n)*[1].*ebuilds/repo2",
			#cpv and slot in repo1 and repo2. repo1 must be used
			"test-repo-selection:4": "(.|\n)*cat-test/test-repo-selection-1.6.*[1](.|\n)*[1].*ebuilds/repo1",
			"=test-repo-selection-1.6:4": "(.|\n)*cat-test/test-repo-selection-1.6.*[1](.|\n)*[1].*ebuilds/repo1",

			#FAILS
			#the ebuilds with this version are in other slot
			"=test-repo-selection-1.0:4": "(.|\n)*there are no ebuilds to satisfy",

		}
	
		nopretend_myopts ={'--verbose': True, '--color': 'n'}
		nopretend_tests = {	
			#checking if the correct ebuild is used
			"test-repo-ebuild::repo1": "(.|\n)*repo1_ok",
			"test-repo-ebuild::gentoo": "(.|\n)*portage_ok",
		}
	
		mult = MultirepoTest()

		for myfile, reg_exp in tests.iteritems():
			result_str = mult.runEmergeTest(action_build, mult.settings, mult.trees, mult.mtimedb, myopts, None, [myfile], mult.spinner)
			self.assertTrue(re.match(reg_exp, result_str), "Repository Selection Error: %s" % myfile)

		for myfile, reg_exp in nopretend_tests.iteritems():
			result_str = mult.runEmergeTest(action_build, mult.settings, mult.trees, mult.mtimedb, nopretend_myopts, None, [myfile], mult.spinner)
			self.assertTrue(re.match(reg_exp, result_str), "Repository Selection Error: %s" % myfile)

