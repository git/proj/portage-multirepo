# test_dep_getslot.py -- Portage Unit Testing Functionality
# Copyright 2006 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
import re
from portage.tests import TestCase
from portage.dep import dep_getrepo
from multirepo_test import MultirepoTest
from  _emerge.actions import action_build

class testPackageFiles(TestCase):
	""" A simple testcase for isvalidatom
	"""

	def testPackageFiles(self):
		myopts ={'--pretend': True, '--verbose': True, '--color': 'n'}
		
		tests= {
			#package.use
			"=test-use-1.0::gentoo": '(.|\n)*USE="-multirepo_test"',
			"=test-use-2.0::gentoo": '(.|\n)*USE="-multirepo_test"',
			"=test-use-3.0::gentoo": '(.|\n)*USE="multirepo_test"',
			"=test-use-1.0::repo1": '(.|\n)*USE="-multirepo_test"',
			"=test-use-2.0::repo1": '(.|\n)*USE="multirepo_test"',
			"=test-use-3.0::repo1": '(.|\n)*USE="multirepo_test"',

			#package.keywords
			"=test-keywords-1.0::gentoo": '(.|\n)*(masked by:.*keyword)',
			"=test-keywords-2.0::gentoo": '(.|\n)*(masked by:.*keyword)',
			"=test-keywords-3.0::gentoo": '(.|\n)*\[ebuild.*cat-packages/test-keywords',
			"=test-keywords-1.0::repo1": '(.|\n)*(masked by:.*keyword)',
			"=test-keywords-2.0::repo1": '(.|\n)*\[ebuild.*cat-packages/test-keywords',
			"=test-keywords-3.0::repo1": '(.|\n)*\[ebuild.*cat-packages/test-keywords',

			"=test-mask-1.0::gentoo": '(.|\n)*\[ebuild.*cat-packages/test-mask',
			"=test-mask-2.0::gentoo": '(.|\n)*\[ebuild.*cat-packages/test-mask',
			"=test-mask-3.0::gentoo": '(.|\n)*(masked by: package.mask)',
			"=test-mask-1.0::repo1": '(.|\n)*\[ebuild.*cat-packages/test-mask',
			"=test-mask-2.0::repo1": '(.|\n)*(masked by: package.mask)',
			"=test-mask-3.0::repo1": '(.|\n)*(masked by: package.mask)',

			"=test-unmask-1.0::gentoo": '(.|\n)*(masked by: package.mask)',
			"=test-unmask-2.0::gentoo": '(.|\n)*(masked by: package.mask)',
			"=test-unmask-3.0::gentoo": '(.|\n)*\[ebuild.*cat-packages/test-unmask',
			"=test-unmask-1.0::repo1": '(.|\n)*(masked by: package.mask)',
			"=test-unmask-2.0::repo1": '(.|\n)*\[ebuild.*cat-packages/test-unmask',

       			#package.properties
			"=test-properties-1.0::gentoo": '(.|\n)*(masked by: interactive properties)',
			"=test-properties-2.0::gentoo": '(.|\n)*(masked by: interactive properties)',
			"=test-properties-3.0::gentoo": '(.|\n)*\[ebuild.*cat-packages/test-properties',
			"=test-properties-1.0::repo1": '(.|\n)*(masked by:.*interactive properties)',
			"=test-properties-2.0::repo1": '(.|\n)*\[ebuild.*cat-packages/test-properties',
			"=test-properties-3.0::repo1": '(.|\n)*\[ebuild.*cat-packages/test-properties',
			
			#package.license
			"=test-license-1.0::gentoo": '(.|\n)*(masked by: TEST license)',
			"=test-license-2.0::gentoo": '(.|\n)*(masked by: TEST license)',
			"=test-license-3.0::gentoo": '(.|\n)*\[ebuild.*cat-packages/test-license',
			"=test-license-1.0::repo1": '(.|\n)*(masked by:.*TEST license)',
			"=test-license-2.0::repo1": '(.|\n)*\[ebuild.*cat-packages/test-license',
			"=test-license-3.0::repo1": '(.|\n)*\[ebuild.*cat-packages/test-license',

		}

		tests_exclude = {
			"=test-exclude-1.0::gentoo": ('(.|\n)*(masked by: exclude option)', ['test-exclude']),
			"test-exclude::gentoo": ('(.|\n)*(masked by: exclude option)', ['cat-packages/test-exclude::gentoo']),
			"test-exclude": ('(.|\n)*cat-packages/test-exclude.*0 kB\n', ['cat-packages/test-exclude::repo1']),
			"test-exclude": ('(.|\n)*cat-packages/test-exclude.*[1](.|\n)*[1].*ebuilds/repo1', ['cat-packages/test-exclude::gentoo']),
		}
		
		mult = MultirepoTest()

		for myfile, reg_exp in tests.iteritems():
			result_str = mult.runEmergeTest(action_build, mult.settings, mult.trees, mult.mtimedb, myopts, None, [myfile], mult.spinner)
			self.assertTrue(re.match(reg_exp, result_str), "Package files Error: %s" % myfile)

		for myfile, (reg_exp, exclude) in tests_exclude.iteritems():
			myopts['--exclude'] = exclude
			result_str = mult.runEmergeTest(action_build, mult.settings, mult.trees, mult.mtimedb, myopts, None, [myfile], mult.spinner)
			self.assertTrue(re.match(reg_exp, result_str), "Package files Error: %s" % myfile)


